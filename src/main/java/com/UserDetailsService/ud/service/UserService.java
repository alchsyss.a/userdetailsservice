package com.UserDetailsService.ud.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.UserDetailsService.ud.shared.dto.UserDto;

public interface UserService extends UserDetailsService {
	UserDto createUser(UserDto user);
}
